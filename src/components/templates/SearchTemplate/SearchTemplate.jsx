import React from 'react';
import PropTypes from 'prop-types';
import DynamicForm from '../../organisms/DynamicForm';
import defaultImage from '../../../images/default-image-small.png';
import './SearchTemplate.scss';

const SearchTemplate = (props) => {
  const { image, formFields } = props;
  return (
    <div className="SearchTemplate">
      <img className="SearchTemplate-Image" src={image} alt="Hero - Mastercity" />
      <DynamicForm
        inputs={formFields}
        submitButton={{ text: 'Search', color: 'primary', fullWidth: true }}
        {...props}
      />
    </div>
  );
};

SearchTemplate.defaultProps = {
  image: defaultImage,
  formInputs: [],
};

SearchTemplate.propTypes = {
  image: PropTypes.node.isRequired,
  formInputs: PropTypes.array,
};

SearchTemplate.propTypes = {};

export default SearchTemplate;
