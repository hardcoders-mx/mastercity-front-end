import React from 'react';
import PropTypes from 'prop-types';
import DynamicForm from "../../organisms/DynamicForm";
import defaultImage from '../../../images/default-image-small.png';
import './SignUpTemplate.scss';

const SignUpTemplate = ({ formFields, image, ...props }) => {
  return (
    <div className="SignUpTemplate">
      <img className="SignUpTemplate-Image" src={image} alt="Mastercity - Sign Up" />
      <DynamicForm
        inputs={formFields}
        submitButton={{ text: 'Sign Up', color: 'primary', fullWidth: true }}
        {...props}
      />
    </div>
  );
};

SignUpTemplate.defaultProps = {
  image: defaultImage,
  formInputs: [],
};

SignUpTemplate.propTypes = {
  image: PropTypes.node.isRequired,
  formInputs: PropTypes.array,
};

export default SignUpTemplate;
