import React from 'react';
import image from '../../../../images/fatal-error.png';
import Typography from '../../../atoms/Typography';
import './ErrorState.scss';

const ErrorState = () => {
  return (
    <div className="ErrorState">
      <div>
        <img className="ErrorState-Image" src={image} alt="" />
        <Typography variant="h1" display="block" align="center" color="error">Whoops, something went wrong!</Typography>
      </div>
    </div>
  );
};

export default ErrorState;
