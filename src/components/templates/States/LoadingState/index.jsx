import React from 'react';
import Loader from '../../../atoms/Loader';
import "./LoadingState.scss";

const LoadingState = () => {
  return (
    <div className="LoadingState">
      <Loader />
    </div>
  );
};

export default LoadingState;
