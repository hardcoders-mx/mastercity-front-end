import React from 'react';
import image from '../../../../images/is-empty.png';
import Typography from '../../../atoms/Typography';
import './EmptyState.scss';

const EmptyState = () => {
  return (
    <div className="EmptyState">
      <div>
        <img className="EmptyState-Image" src={image} alt="" />
        <Typography variant="h1" display="block" align="center" color="info">
          No Results
        </Typography>
      </div>
    </div>
  );
};

export default EmptyState;
