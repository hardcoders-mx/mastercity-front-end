import React from 'react';
import PropTypes from 'prop-types';
import DynamicForm from "../../organisms/DynamicForm";
import defaultImage from '../../../images/default-image-small.png';
import './LoginTemplate.scss';

const LoginTemplate = (props) => {
  const { image, formFields } = props;
  return (
    <div className="LoginTemplate">
      <img className="LoginTemplate-Image" src={image} alt="Mastercity - Sign Up" />
      <DynamicForm
        inputs={formFields}
        submitButton={{ text: 'Login', color: 'primary', fullWidth: true }}
        {...props}
      />
    </div>
  );
};

LoginTemplate.defaultProps = {
  image: defaultImage,
  formInputs: [],
};

LoginTemplate.propTypes = {
  image: PropTypes.node.isRequired,
  formInputs: PropTypes.array,
};

export default LoginTemplate;
