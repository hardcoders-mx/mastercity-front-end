import React from 'react';
// import PropTypes from 'prop-types';
import './PropertyTemplate.scss';
import Modal from '../../molecules/Modal';
import CardHeader from '../../atoms/CardHeader';
import CardContent from '../../atoms/CardContent';
import Button from '../../atoms/Button';
import Slider from '../../organisms/SliderCarousel';

const yesOrNotStr = (validation) => (validation ? 'Yes' : 'Not');

const Description = ({ address, property }) => {
  const { colony, street, state, townHall, country, interiorNumber, outdoorNumber, postalCode } = address;
  const {
    priceMeters,
    propertyType,
    operationType,
    furnish,
    parking,
    swimmingPool,
    heating,
    security,
    cellar,
    elevator,
  } = property;

  return (
    <>
      <p>Address:</p>
      <p>
        {street}, {colony}, {state}, {townHall}, {country}, {interiorNumber}, {outdoorNumber}, {postalCode}
      </p>
      <p style={{ textAlign: 'left' }}>Details:</p>
      <ul style={{ textAlign: 'left' }}>
        <li style={{ textTransform: 'capitalize' }}>Price per meters: ${priceMeters}</li>
        <li style={{ textTransform: 'capitalize' }}>Property type: {propertyType}</li>
        <li style={{ textTransform: 'capitalize' }}>Operation type: {operationType} </li>
        <li>Furnish: {yesOrNotStr(furnish)}</li>
        <li>Parking: {yesOrNotStr(parking)}</li>
        <li>Swimming pool: {yesOrNotStr(swimmingPool)}</li>
        <li>Heating: {yesOrNotStr(heating)}</li>
        <li>Security: {yesOrNotStr(security)}</li>
        <li>Cellar: {yesOrNotStr(cellar)}</li>
        <li>Elevator: {yesOrNotStr(elevator)}</li>
      </ul>
    </>
  );
};

const PropertyTemplate = (props) => {
  const { onClick, onClose, isModalOpen, property } = props;
  const { mediaFiles, price, squareMeters, rooms, bathrooms, parking, address, offerer } = property;
  const { email, phone, firstName, lastName, isRealEstate, avatar } = offerer;

  const fullOfferer = isRealEstate
    ? `Real State - ${firstName} ${lastName}`
    : `Direct Deal - ${firstName} ${lastName}`;
  const headerProps = { price, coin: 'MXN', offerer: fullOfferer, image: mediaFiles[0].secure_url, avatar };
  const contentProps = {
    // description: '',
    featureOne: `${squareMeters} m2`,
    featureTwo: `${rooms} Rooms`,
    featureThree: `${bathrooms} Bathrooms`,
    featureFour: `Parking: ${yesOrNotStr(parking)}`,
  };

  return (
    <div className="PropertyTemplate">
      <Slider images={mediaFiles} />
      <CardHeader {...headerProps} />
      <CardContent {...contentProps}>
        <Button variant="outlined" color="secondary" size="medium" onClick={onClick}>
          Contact
        </Button>
        <Description address={address} property={property} />
        <br />
        <br />
        <br />
        <br />
        <br />
      </CardContent>
      {isModalOpen && (
        <Modal onClose={onClose} style={{ width: 400, textAlign: 'center' }}>
          <p>
            <b>Offerer:</b> {fullOfferer}
          </p>
          <p>
            <b>Email:</b> {email}
          </p>
          <p>
            <b>Phone Number:</b> {phone}
          </p>
        </Modal>
      )}
    </div>
  );
};

PropertyTemplate.defaultProps = {};

PropertyTemplate.propTypes = {};

export default PropertyTemplate;
