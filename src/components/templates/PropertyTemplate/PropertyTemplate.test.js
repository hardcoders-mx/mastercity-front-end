import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import PropertyTemplate from './PropertyTemplate';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<PropertyTemplate />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<PropertyTemplate />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<PropertyTemplate className="PropertyTemplate"/>);
  expect(component.find('div').hasClass('PropertyTemplate')).toBe(true);
});
