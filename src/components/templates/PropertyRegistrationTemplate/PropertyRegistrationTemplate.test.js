import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import PropertyRegistrationTemplate from './PropertyRegistrationTemplate';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<PropertyRegistrationTemplate />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<PropertyRegistrationTemplate />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<PropertyRegistrationTemplate className="PropertyRegistrationTemplate"/>);
  expect(component.find('div').hasClass('PropertyRegistrationTemplate')).toBe(true);
});
