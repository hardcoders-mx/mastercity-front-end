import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import PropertiesListTemplate from './PropertiesListTemplate';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<PropertiesListTemplate />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<PropertiesListTemplate />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<PropertiesListTemplate className="PropertiesListTemplate"/>);
  expect(component.find('div').hasClass('PropertiesListTemplate')).toBe(true);
});
