import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import AccountTemplate from './AccountTemplate';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<AccountTemplate />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<AccountTemplate />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<AccountTemplate className="AccountTemplate"/>);
  expect(component.find('div').hasClass('AccountTemplate')).toBe(true);
});
