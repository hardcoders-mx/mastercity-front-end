import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import PropertyMapTemplate from './PropertyMapTemplate';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<PropertyMapTemplate />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<PropertyMapTemplate />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<PropertyMapTemplate className="PropertyMapTemplate"/>);
  expect(component.find('div').hasClass('PropertyMapTemplate')).toBe(true);
});
