import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import DashboardTemplate from './DashboardTemplate';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<DashboardTemplate />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<DashboardTemplate />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<DashboardTemplate className="DashboardTemplate"/>);
  expect(component.find('div').hasClass('DashboardTemplate')).toBe(true);
});
