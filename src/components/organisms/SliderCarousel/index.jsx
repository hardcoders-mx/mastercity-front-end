import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader

// this image is an example
import defaultImage from '../../../images/default-image-small.png';

const SliderCarousel = ({ images, image }) => {
  return (
    <div>
      <Carousel>
        {images &&
          images.length > 0 &&
          images.map((image, index) => (
            <div key={index}>
              <img src={image.secure_url} />
              <p className="legend">Legend {index}</p>
            </div>
          ))}
      </Carousel>
    </div>
  );
};

SliderCarousel.defaultProps = {
  image: defaultImage,
};

export default SliderCarousel;
