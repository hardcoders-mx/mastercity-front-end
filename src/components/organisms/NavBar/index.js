import React from 'react'
import { Link, Nav } from './styles'
import { MdExplore, MdSearch ,MdFavoriteBorder, MdPersonOutline } from 'react-icons/md'
import { routes } from "../../../routes";

const SIZE = '32px'

export const NavBar = () => {
    return (
        <Nav>
            <Link to={routes.properties.path}><MdExplore size={SIZE} /></Link>
            <Link to={routes.search.path}><MdSearch size={SIZE} /></Link>
            <Link to={routes.favorites.path}><MdFavoriteBorder size={SIZE} /></Link>
            <Link to={routes.account.path}><MdPersonOutline size={SIZE} /></Link>
        </Nav>
    )
}
