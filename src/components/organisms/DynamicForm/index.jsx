import React from 'react';
import Field from '../../atoms/Field';
import Select from '../../atoms/Select';
import RadioButtonsAttached from '../../atoms/RadioButtonsAttached';
import Button from '../../atoms/Button';

const DynamicForm = (props) => {
  const {
    values,
    errors,
    touched,
    handleBlur,
    handleChange,
    handleSubmit,
    setFieldValue,
    inputs,
    submitButton,
    style,
  } = props;
  const { text, color, fullWidth } = submitButton;

  const renderInputs = (input) => {
    const { component, name, label, type, placeholder, startIcon, endIcon, handleIconAction, options } = input;
    switch (component) {
      case 'text':
        return (
          <Field
            key={name}
            type={type}
            name={name}
            label={label}
            placeholder={placeholder}
            disabled={false}
            value={values[name]}
            onBlur={handleBlur}
            onChange={handleChange}
            helperText={touched[name] ? errors[name] : ''}
            status={touched[name] && (errors[name] ? 'invalid' : 'valid')}
            startIcon={startIcon}
            endIcon={endIcon}
            handleIconAction={handleIconAction}
          />
        );
      case 'select':
        return (
          <Select
            // type={type}
            key={name}
            name={name}
            label={label}
            placeholder={placeholder}
            disabled={false}
            value={values[name]}
            onBlur={handleBlur}
            onChange={handleChange}
            helperText={touched[name] ? errors[name] : ''}
            status={touched[name] && (errors[name] ? 'invalid' : 'valid')}
            startIcon={startIcon}
            endIcon={endIcon}
            handleIconAction={handleIconAction}
            options={options}
            setFieldValue={setFieldValue}
          />
        );
      case 'radioAttached':
        return (
          <RadioButtonsAttached
            key={name}
            name={name}
            value={values[name]}
            onBlur={handleBlur}
            onChange={handleChange}
            helperText={touched[name] ? errors[name] : ''}
            status={touched[name] && (errors[name] ? 'invalid' : 'valid')}
            options={options}
          />
        );
      default:
        return null;
    }
  };

  return (
    <form className="DynamicForm" onSubmit={handleSubmit} style={style}>
      {inputs.map((input) => renderInputs(input))}
      <Button type="submit" color={color} margin="none" fullWidth={fullWidth}>
        {text}
      </Button>
    </form>
  );
};

export default DynamicForm;
