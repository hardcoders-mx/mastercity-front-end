import React from 'react';
import { mdiChartGantt, mdiReorderHorizontal } from '@mdi/js';
import Icon from '@mdi/react';
import Logo from '../../../images/Imagotipo.png';
import './NavMenu.scss';

const NavMenu = (props) => {
  const handleShowSettings = () => {
    console.log('handleShowSettings');
  };

  const handleShowMenu = () => {
    console.log('handleShowMenu');
  };

  return (
    <div className="Navbar">
      <div className="Navbar-Image">
        <img src={Logo} alt="Mastercity-logo" width="150" height="70" />
      </div>
      <div className="Navbar-IconSettings">
        <Icon path={mdiChartGantt} onClick={handleShowSettings} title="User Profile" color={'white'} size={2} />
      </div>
      <div className="Navbar-IconMenu">
        <Icon path={mdiReorderHorizontal} onClick={handleShowMenu} title="User Profile" color={'white'} size={2} />
      </div>
    </div>
  );
};

export default NavMenu;
