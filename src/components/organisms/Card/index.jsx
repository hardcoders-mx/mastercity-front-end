import React from 'react';
import CardContainer from '../../atoms/CardContainer';
import CardHeader from '../../atoms/CardHeader';
import CardContent from '../../atoms/CardContent';
import CardActions from '../../atoms/CardActions';

const Card = (props) => {
  const {
    image,
    avatar,
    price,
    coin,
    offerer,
    description,
    featureOne,
    featureTwo,
    featureThree,
    featureFour,
    handleShow,
    handleContact,
  } = props;
  const headerProps = { price, coin, offerer, image, avatar };
  const contentProps = { description, featureOne, featureTwo, featureThree, featureFour };
  const actionsProps = { handleShow, handleContact };

  return (
    <CardContainer>
      <CardHeader {...headerProps} />
      <CardContent {...contentProps} />
      <CardActions {...actionsProps} />
    </CardContainer>
  );
};

export default Card;
