/* PLOP_INJECT_IMPORT */
import DashboardTemplate from './templates/DashboardTemplate';
import AdminTemplate from './templates/AdminTemplate';
import BrokerTemplate from './templates/BrokerTemplate';
import RealEstateTemplate from './templates/RealEstateTemplate';
import AccountTemplate from './templates/AccountTemplate';
import FavoritesTemplate from './templates/FavoritesTemplate';
import SearchTemplate from './templates/SearchTemplate';
import PropertyMapTemplate from './templates/PropertyMapTemplate';
import PropertyTemplate from './templates/PropertyTemplate';
import PropertyRegistrationTemplate from './templates/PropertyRegistrationTemplate';
import PropertiesMapTemplate from './templates/PropertiesMapTemplate';
import PropertiesListTemplate from './templates/PropertiesListTemplate';
import AccountRecoveryTemplate from './templates/AccountRecoveryTemplate';
import SignUpTemplate from './templates/SignUpTemplate';
import LoginTemplate from './templates/LoginTemplate';
import HomeTemplate from './templates/HomeTemplate';
import LoadingState from './templates/States/LoadingState';
import ErrorState from './templates/States/ErrorState';
import EmptyState from './templates/States/EmptyState';

export {
  /* PLOP_INJECT_EXPORT */
  DashboardTemplate,
  AdminTemplate,
  BrokerTemplate,
  RealEstateTemplate,
  AccountTemplate,
  FavoritesTemplate,
  SearchTemplate,
  PropertyMapTemplate,
  PropertyTemplate,
  PropertyRegistrationTemplate,
  PropertiesMapTemplate,
  PropertiesListTemplate,
  AccountRecoveryTemplate,
  SignUpTemplate,
  LoginTemplate,
  HomeTemplate,
  LoadingState,
  ErrorState,
  EmptyState,
};
