import React, { useContext } from 'react';
import { createPortal } from 'react-dom';
import Button from '../../atoms/Button';
import { ModalContext } from '../../../containers/ModalContextProvider';
import './Modal.scss';

const Modal = ({ onClose, children, buttonType, buttonText, buttonSize, ...props }) => {
  const modalNode = useContext(ModalContext);
  return createPortal(
    <div className="Modal-Overlay">
      <div className="Modal-Dialog" {...props}>
        {children}
        <Button variant="outlined" color="secondary" type={buttonType} size={buttonSize} onClick={onClose}>
          {buttonText}
        </Button>
      </div>
    </div>,
    modalNode,
  );
};

Modal.defaultProps = {
  buttonSize: 'small',
  buttonType: 'button',
  buttonText: 'OK',
  onClose: () => {},
};

export default Modal;
