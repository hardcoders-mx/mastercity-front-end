import React, { useState } from 'react';
import axios from 'axios';

const UploadImage = () => {
  const [selectedFile, setSelectedFile] = useState(null);

  const fileSelectedHandler = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const fileUploadHandler = (e) => {
    console.log(e);
    const fd = new FormData();
    fd.append('image', selectedFile, selectedFile.name);
    //example url to upload image
    axios
      .post('https://mastercity.xyz/uploads', fd, {
        onUploadProgress: (ProgressEvent) => {
          console.log(ProgressEvent);
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <input type="file" onChange={fileSelectedHandler} />
      <button onClick={fileUploadHandler}>Upload</button>
    </div>
  );
};

export default UploadImage;
