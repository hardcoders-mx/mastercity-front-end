import React from 'react';
import PropsTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiAlertCircle, mdiCheckBold } from '@mdi/js';
import { success, danger, primaryMain } from '../../../utils/colors';
import Typography from '../Typography';
import './Field.scss';

const renderIcon = (status, startIcon) => {
  switch (status) {
    case 'valid':
      return <Icon path={mdiCheckBold} color={success} />;
    case 'invalid':
      return <Icon path={mdiAlertCircle} color={danger} />;
    default:
      return startIcon ? <Icon path={startIcon} color={primaryMain} /> : null;
  }
};

const getColorStyle = (status) => {
  switch (status) {
    case 'valid':
      return 'success';
    case 'invalid':
      return 'error';
    default:
      return 'primary';
  }
};

const Field = ({
  type,
  name,
  label,
  startIcon,
  endIcon,
  onBlur,
  onChange,
  value,
  placeholder,
  helperText,
  status,
  handleIconAction,
}) => {
  return (
    <div className={`Field Field_status_${status}`}>
      <span className="Field-StartIcon">{renderIcon(status, startIcon)}</span>
      <div>
        <label className={`Field-Label Field-Label_status_${status}`} htmlFor={name}>
          {label}
        </label>
        <input
          className="Field-Input"
          type={type}
          id={name}
          name={name}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          placeholder={placeholder}
        />
        <Typography variant="caption" color={getColorStyle(status)}>
          {helperText}
        </Typography>
      </div>
      <span className="Field-EndIcon">
        {endIcon && (
          <Icon className="Field-EndIcon_icon" path={endIcon} color={primaryMain} onClick={handleIconAction} />
        )}
      </span>
    </div>
  );
};

Field.defaultProps = {
  type: 'text',
  status: '',
  handleIconAction: () => {},
};

Field.propsTypes = {
  type: PropsTypes.string,
  name: PropsTypes.string,
  label: PropsTypes.string,
  status: PropsTypes.string,
  helperText: PropsTypes.string,
  placeholder: PropsTypes.string,
  startIcon: PropsTypes.node,
  endIcon: PropsTypes.node,
  onChange: PropsTypes.func,
  handleIconAction: PropsTypes.func,
  value: PropsTypes.any,
};

export default Field;
