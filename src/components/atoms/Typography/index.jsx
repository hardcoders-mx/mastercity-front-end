import React from "react";
import PropTypes from 'prop-types';
import "./Typography.scss";

const variantMapping = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  h6: "h6",
  subtitle: "h2",
  body: "p",
  caption: "span",
  button: "span",
  overline: "span",
};

const Typography = ({ component, children, variant, color, align, display }) => {
  const colorStyle = `Typography_color_${color}`;
  const variantStyle = `Typography_variant_${variant}`;
  const alignStyle = `Typography_align_${align}`;
  const displayStyle = `Typography_display_${display}`;

  const Component = component ? component : variantMapping[variant];

  return (
    <Component className={`Typography ${colorStyle} ${variantStyle} ${alignStyle} ${displayStyle}`}>
      {children}
    </Component>
  );
};

Typography.defaultProps = {
  variant: "body",
  color: "initial",
  align: "inherit",
  display: "initial",
};

Typography.propTypes = {
  // children: PropTypes.element.isRequired,
  component: PropTypes.node,
  variant: PropTypes.oneOf([
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "subtitle",
    "body",
    "caption",
    "button",
    "overline",
  ]),
  color: PropTypes.oneOf([
    "initial",
    "inherit",
    "primary",
    "secondary",
    "primaryContrastText",
    "secondaryContrastText",
    "success",
    "error",
    "warning",
    "info",
    "black",
    "white",
  ]),
  align: PropTypes.oneOf([
    "inherit",
    "left",
    "center",
    "right",
    "justify",
  ]),
  display: PropTypes.oneOf([
    "initial",
    "block",
    "inline",
  ]),
};

export default Typography;
