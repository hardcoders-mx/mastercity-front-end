import React from 'react';

import './CardContainer.scss';

/**
 * * Main card container
 * @param get a other components like props children CardActions, CardHeader
 */

const CardContainer = (props) => {
  return <div className="Card-container">{props.children}</div>;
};

export default CardContainer;
