import React from 'react';
import defaultImage from '../../../images/default-image-small.png';
import avatar from './mexico.svg';
import './CardHeader.scss';

/**
 * * Main card header
 * Comments
 * @param props get redux or HOC
 */

const CardHeader = (props) => {
  const { price, coin, offerer, image, avatar } = props;

  return (
    <div className="Card-header">
      <figure className="Card-header-figure">
        <img src={image ? image : defaultImage} alt="LogoImage" />
      </figure>
      <div className="Card-header-figureIcon">
        <img width="40" src={avatar} alt="ImageIcon" />
      </div>
      <div className="Card-header-description">
        <h6 className="Card-header-price">
          ${price} {coin}
        </h6>
        <span className="Card-header-direction"> {offerer} </span>
      </div>
    </div>
  );
};

CardHeader.defaultProps = {
  image: defaultImage,
  avatar: avatar,
  coin: 'MXN'
}

export default CardHeader;
