import React, { useEffect, useRef } from 'react';
import { Map, TileLayer, Popup, Marker } from 'react-leaflet';
import L from 'leaflet';
import './leaflet.scss';

/* example data JSON with coordinates */
import nationalParks from './national-parks.json';

const styles = {
  wrapper: {
    height: 'calc(100vh - 85px)',
    width: '90%',
    margin: '0 auto',
    display: 'flex',
  },
  map: {
    flex: 1,
  },
};

const position = [19.4267211, -99.1696819];

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('./images/marker-icon-2x.png'),
  iconUrl: require('./images/marker-icon.png'),
  shadowUrl: require('./images/marker-shadow.png'),
});

/**
 *
 * @param props {*} default Map props are in the bottom
 */

const MainMap = (props) => {
  const mapRef = useRef();

  useEffect(() => {
    const { current = {} } = mapRef;
    const { leafletElement: map } = current;
    if (!map) return;

    const parkGeoJson = new L.GeoJSON(nationalParks, {
      /**
       * this section only destructuring the name or properties of the JSON example
       */
      onEachFeature: (feature = {}, layer) => {
        const { properties = {} } = feature;
        const { Name } = properties;
        if (!Name) return;
        layer.bindPopup(`<p>${Name}</p>`);
      },
    });
    parkGeoJson.addTo(map);
  }, []);

  return (
    <div style={styles.wrapper}>
      <Map ref={mapRef} style={styles.map} center={props.center} zoom={props.zoom}>
        <TileLayer url={props.url} />
        <Marker position={position}>
          <Popup>
            <span>
              Platzi México. <br />
            </span>
          </Popup>
        </Marker>
      </Map>
    </div>
  );
};

MainMap.defaultProps = {
  center: [19.3906797, -99.2840402],
  zoom: 5,
  url: 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
};

export default MainMap;
