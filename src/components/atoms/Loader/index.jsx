import React from 'react';
import './Loader.scss';

/**
 * * Spinner when the page or component is charging
 */

export default function Loader() {
  return (
    <div className="container-spinner">
      <div className="spinner">
        <div className="rect1"></div>
        <div className="rect2"></div>
        <div className="rect3"></div>
        <div className="rect4"></div>
        <div className="rect5"></div>
      </div>
    </div>
  );
}
