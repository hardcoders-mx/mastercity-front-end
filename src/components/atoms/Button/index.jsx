import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

const variantMapping = {
  outlined: 'Outlined',
  contained: 'Contained',
  flattened: 'Flattened',
  circular: 'Circular',
};

const Button = ({
  children,
  typeButton,
  type,
  variant,
  color,
  size,
  margin,
  loading,
  disabled,
  startIcon,
  endIcon,
  fullWidth,
  onClick,
  // attached,
}) => {
  const typeStyle = `Button${variantMapping[variant]}_type_${typeButton}`;
  const colorStyle = `Button${variantMapping[variant]}_color_${color}`;
  const sizeStyle = `Button${variantMapping[variant]}_size_${size}`;
  const widthStyle = `${fullWidth ? 'fullWidth' : ''}`;
  const marginStyle = `Button${variantMapping[variant]}_margin_${margin}`;
  // const attachedStyle = `Button${variantMapping[variant]}_attached_${attached}`;

  return (
    <button
      type={type}
      className={`Button${variantMapping[variant]} ${typeStyle} ${colorStyle} ${sizeStyle} ${marginStyle} ${widthStyle}`}
      disabled={disabled}
      onClick={onClick}
    >
      <span className="Button-StartIcon">{startIcon}</span>
      {!loading ? children : '<ProgressState variant="circular" />'}
      <span className="Button-EndIcon">{endIcon}</span>
    </button>
  );
};

Button.defaultProps = {
  type: 'button',
  typeButton: 'rounded',
  variant: 'contained',
  color: 'default',
  size: 'medium',
  attached: 'none',
  margin: 'normal',
  disabled: false,
  loading: false,
  fullWidth: false,
};

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  type: PropTypes.string,
  typeButton: PropTypes.oneOf(['rounded', 'circular', 'straight']),
  variant: PropTypes.oneOf(['flattened', 'outlined', 'contained']),
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  attached: PropTypes.oneOf(['none', 'left', 'right']),
  margin: PropTypes.oneOf(['none', 'normal', 'dense']),
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  startIcon: PropTypes.node,
  endIcon: PropTypes.node,
};

export default Button;
