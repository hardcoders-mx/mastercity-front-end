import React from 'react';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import './tab.scss';

/* import the map like example */
//import Map from '../atom-map/index';

const CustomTab = ({ children }) => (
  <Tab>
    <p>{children}</p>
  </Tab>
);

CustomTab.tabsRole = 'Tab'; // Required field to use your custom Tab

/**
 * Custom tab you can add N tabs to show like list
 */
const MainTab = () => {
  return (
    <>
      <Tabs>
        <TabList>
          <CustomTab>List</CustomTab>
          <CustomTab>Map</CustomTab>
        </TabList>
        <TabPanel>
          <p>Test text</p>
        </TabPanel>
        <TabPanel>Test text {/* <Map /> */}</TabPanel>
      </Tabs>
    </>
  );
};

export default MainTab;
