import React, { Fragment } from 'react';
import './RadioButtonsAttached.scss';

const RadioButtonsAttached = (props) => {
  const { options, name, value, onChange, onBlur, status } = props;
  const statusStyle = `RadioButtonsAttached_status_${status}`;

  return (
    <div className={`RadioButtonsAttached ${statusStyle}`} onChange={onChange} onBlur={onBlur}>
      {options.map((option, index) => (
        <Fragment key={`${name}${index}`}>
          <input
            type="radio"
            name={name}
            id={`${name}${index}`}
            value={option.value}
            checked={value === option.value}
          />
          <label for={`${name}${index}`}>{option.label}</label>
        </Fragment>
      ))}
    </div>
  );
};

export default RadioButtonsAttached;
