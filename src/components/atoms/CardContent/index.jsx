import React from 'react';
import './CardContent.scss';

/**
 * * Main card content
 * @param props
 */

const CardContent = (props) => {
  const { description, featureOne, featureTwo, featureThree, featureFour, children } = props;

  return (
    <div className="CardContent">
      <div className="CardContent-Features">
        <div> {featureOne} </div>
        <div>|</div>
        <div> {featureTwo} </div>
        <div>|</div>
        <div> {featureThree} </div>
        <div>|</div>
        <div> {featureFour} </div>
      </div>
      <div className="CardContent-Description">
        <p>{description}</p>
        {children}
      </div>
    </div>
  );
};

export default CardContent;
