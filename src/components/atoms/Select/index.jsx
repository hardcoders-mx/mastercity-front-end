import React, { useState, useEffect } from 'react';
import PropsTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiAlertCircle, mdiCheckBold, mdiChevronDown, mdiChevronUp } from '@mdi/js';
import { success, danger, primaryMain } from '../../../utils/colors';
import Typography from '../Typography';
import './Select.scss';

const renderIcon = (status, startIcon) => {
  switch (status) {
    case 'valid':
      return <Icon path={mdiCheckBold} color={success} />;
    case 'invalid':
      return <Icon path={mdiAlertCircle} color={danger} />;
    default:
      return startIcon ? <Icon path={startIcon} color={primaryMain} /> : null;
  }
};

const getColorStyle = (status) => {
  switch (status) {
    case 'valid':
      return 'success';
    case 'invalid':
      return 'error';
    default:
      return 'primary';
  }
};

const Select = ({
  type,
  name,
  label,
  startIcon,
  onBlur,
  value,
  placeholder,
  helperText,
  status,
  options,
  setFieldValue,
}) => {
  const [isCollapsed, setIsCollapsed] = useState(true);
  const [activeOption, setActiveOption] = useState(-1);
  const endIcon = isCollapsed ? mdiChevronDown : mdiChevronUp;

  useEffect(() => {
    if (activeOption >= 0) {
      setFieldValue(name, options[activeOption].value);
    }
  }, [activeOption])

  const toggleCollapse = () => {
    document.getElementById(name).focus();
    setIsCollapsed(!isCollapsed);
  };

  const handleKeyDown = (event) => {
    switch (event.key) {
      case 'ArrowDown':
        setActiveOption(activeOption === options.length - 1 ? activeOption : activeOption + 1);
        break;
      case 'ArrowUp':
        setActiveOption(activeOption === 0 ? activeOption : activeOption - 1);
        break;
      case 'Enter':
        toggleCollapse()
        break;
      default:
        break;
    }
  };

  const handleClick = (event) => {
    const { innerText } = event.target;
    const index = options.findIndex((option) => option.label === innerText);
    setFieldValue(name, options[index].value);
    setActiveOption(index);
    setIsCollapsed(true);
  };

  return (
    <div className={`Select Select_status_${status}`}>
      <span className="Select-StartIcon">{renderIcon(status, startIcon)}</span>
      <div>
        <label className={`Select-Label Select-Label_status_${status}`} htmlFor={name}>
          {label}
        </label>
        <div>
          <input
            className="Select-Input"
            type={type}
            id={name}
            name={name}
            onBlur={onBlur}
            value={value}
            placeholder={placeholder}
            onKeyDown={handleKeyDown}
            onFocus={toggleCollapse}
            onChange={() => {}}
          />
          {options.length > 0 && !isCollapsed && (
            <ul className="Select-Options">
              {options.map((option, index) => {
                return (
                  <li
                    key={option.value}
                    onClick={handleClick}
                    className={index === activeOption ? 'Select-Option_active' : 'Select-Option_inactive'}
                  >
                    {option.label}
                  </li>
                );
              })}
            </ul>
          )}
        </div>
        <Typography variant="caption" color={getColorStyle(status)}>
          {helperText}
        </Typography>
      </div>
      <span className="Select-EndIcon">
        {<Icon className="Select-EndIcon_icon" path={endIcon} color={primaryMain} onClick={toggleCollapse} />}
      </span>
    </div>
  );
};

Select.defaultProps = {
  type: 'text',
  status: '',
  options: [],
  placeholder: 'Select an option',
};

Select.propsTypes = {
  type: PropsTypes.string,
  name: PropsTypes.string,
  label: PropsTypes.string,
  status: PropsTypes.string,
  helperText: PropsTypes.string,
  placeholder: PropsTypes.string,
  startIcon: PropsTypes.node,
  value: PropsTypes.any,
  options: PropsTypes.array.isRequired,
};

export default Select;
