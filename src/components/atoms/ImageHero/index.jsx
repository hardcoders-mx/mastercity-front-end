import React from 'react';
import './ImageHero.scss';

const ImageHero = () => {
    return (
        <section className="masthead" role="img" aria-label="Image Description">
            <h1>
            Master City
            </h1>
        </section>
    );
}

export default ImageHero;
