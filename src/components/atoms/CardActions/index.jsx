import React from 'react';
import './CardActions.scss';

/**
 * * Main card actions
 * Comments actions to get data
 */

const CardActions = ({ handleShow, handleContact }) => {
  return (
    <div className="Card-action">
      <button className="Card-action-button" onClick={handleShow}>
        SHOW
      </button>
      <button className="Card-action-button" onClick={handleContact}>
        CONTACT
      </button>
    </div>
  );
};

export default CardActions;
