import ModalContextProvider, { ModalContext } from './ModalContextProvider';

export { ModalContext }

export default ModalContextProvider;
