import React, { Component } from 'react';
import { bool, object, array } from "prop-types";
import { LoadingState, ErrorState, EmptyState } from '../components';

const withHandlingRendering = (WrapperComponent) => {
  class HandlingRendering extends Component {
    static defaultProps = {
      loading: false,
      error: false,
      data: [],
      loadingProps: {},
      errorProps: {},
      emptyProps: {},
    };

    static propTypes = {
      loading: bool,
      error: bool,
      data: array,
      loadingProps: object,
      errorProps: object,
      emptyProps: object,
    }

    render() {
      const { errorProps, loadingProps, emptyProps, error, loading, data } = this.props;
      const newProps = {};

      if (error) {
        return <ErrorState {...errorProps} />;
      }

      if (loading) {
        return <LoadingState {...loadingProps} />;
      }

      if (data && data.length === 0) {
        return <EmptyState {...emptyProps} />;
      }

      return <WrapperComponent {...this.props} {...newProps} />;
    }
  }

  return HandlingRendering;
};

export default withHandlingRendering;
