import React from 'react';
import welcomeImage from '../../images/welcome.png';
import Typography from '../../components/atoms/Typography';
import './WelcomePage.scss';

const WelcomePage = () => {
  return (
    <div className="WelcomePage">
      <div>
        <Typography display="block" align="center" variant="h1">Welcome!</Typography>
        <img className="WelcomePage-Image" src={welcomeImage} alt="" />
        <Typography>Please confirm your email address</Typography>
        <br/>
        <Typography component="ol">
          <li>Check your inbox for the email address associated with your Pinterest account</li>
          <li>Look for a message with the subject line "Please confirm your email"</li>
          <li>Open the email and click Confirm your email</li>
        </Typography>
      </div>
    </div>
  );
};

export default WelcomePage;
