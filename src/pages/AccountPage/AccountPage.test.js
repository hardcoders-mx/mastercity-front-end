import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import AccountPage from './AccountPage';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<AccountPage />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<AccountPage />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<AccountPage className="AccountPage"/>);
  expect(component.find('div').hasClass('AccountPage')).toBe(true);
});
