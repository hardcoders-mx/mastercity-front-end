import React from 'react';
import './AccountPage.scss';

const AccountPage = () => {
  return (
      <div className="main-container">
        <div className="profile">
          <div className="profile-avatar">
            <img src="https://en.gravatar.com/userimage/100726031/cb9b9dbcdd56c304bb76efe11ec19455.jpg" alt="avatar pic"
                 className="profile-img" />
              <div className="profile-name">Cristobal Vega</div>
          </div>
          <img
              src="https://images.unsplash.com/photo-1590229675207-bd4d570ad0f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80"
              alt="" className="profile-cover" />
        </div>
      </div>
  );
};

export default AccountPage;

