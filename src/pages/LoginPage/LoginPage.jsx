import React, { useState } from 'react';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { mdiEmail, mdiLock, mdiEye, mdiEyeOff } from '@mdi/js';
import signUpImage from '../../images/sign-up.png';
import LoginTemplate from '../../components/templates/LoginTemplate';
import { doLoginRequest } from '../../store/modules/auth/actions';
import { routes } from "../../routes";
import './LoginPage.scss';

const validationSchema = object().shape({
  username: string().email().required().label('Email'),
  password: string().required().label('Password'),
});

const formFields = (endIcon, handleIconAction, type) => [
  { name: 'username', label: 'Email', type: 'email', component: 'text', startIcon: mdiEmail },
  { name: 'password', label: 'Password', type, component: 'text', startIcon: mdiLock, endIcon, handleIconAction },
];


const LoginPage = (props) => {
  const [showPassword, setShowPassword] = useState(false);
  const endIconPassword = showPassword ? mdiEyeOff : mdiEye;
  const typePassword = showPassword ? 'text' : 'password';
  const { auth, doLoginRequest } = props;

  const handleSubmit = (values, actions) => {
    console.log("handleSubmit -> values", values)
    try {
      doLoginRequest(values);
    } catch (error) {
      console.log('handleSubmit -> error', error);
    }
  };

  const handleIconAction = () => setShowPassword(!showPassword);

  if (auth.isLogged) {
    return <Redirect to={routes.account.path} />
  }

  return (
    <div className="LoginPage">
      <Formik
        initialValues={{ username: '', password: '' }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => (
          <LoginTemplate
            {...props}
            image={signUpImage}
            formFields={formFields(endIconPassword, handleIconAction, typePassword)}
          />
        )}
      </Formik>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ doLoginRequest }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
