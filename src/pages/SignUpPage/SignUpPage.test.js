import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import SignUpPage from './SignUpPage';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<SignUpPage />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<SignUpPage />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<SignUpPage className="SignUpPage"/>);
  expect(component.find('div').hasClass('SignUpPage')).toBe(true);
});
