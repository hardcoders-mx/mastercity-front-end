import React, { useState } from 'react';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { mdiCardAccountDetails, mdiEmail, mdiLock, mdiEye, mdiEyeOff } from '@mdi/js';
import signUpImage from '../../images/sign-up.png';
import SignUpTemplate from '../../components/templates/SignUpTemplate';
import { doSignUpRequest } from '../../store/modules/auth/actions';
import './SignUpPage.scss';

const validationSchema = object().shape({
  firstName: string().required().label('First name'),
  lastName: string().required().label('Last name'),
  email: string().email().required().label('Email'),
  password: string().min(6).required().label('Password'),
});

const formFields = (endIcon, handleIconAction, type) => [
  { name: 'firstName', label: 'First Name', type: 'text', component: 'text', startIcon: mdiCardAccountDetails },
  { name: 'lastName', label: 'Last Name', type: 'text', component: 'text', startIcon: mdiCardAccountDetails },
  { name: 'email', label: 'Email', type: 'email', component: 'text', startIcon: mdiEmail },
  { name: 'password', label: 'Password', type, component: 'text', startIcon: mdiLock, endIcon, handleIconAction },
];

const SignUpPage = (props) => {
  const [showPassword, setShowPassword] = useState(false);
  const endIconPassword = showPassword ? mdiEyeOff : mdiEye;
  const typePassword = showPassword ? 'text' : 'password';
  const { doSignUpRequest } = props;

  const handleSubmit = (values, actions) => {
    try {
      const user = doSignUpRequest(values);
      console.log('handleSubmit -> user', user);
    } catch (error) {
      console.log('handleSubmit -> error', error);
    }
  };

  const handleIconAction = () => setShowPassword(!showPassword);

  if (props.auth.isLogged) {
    return <Redirect to="/welcome" />
  }

  return (
    <div className="SignUpPage">
      <Formik
        initialValues={{ firstName: '', lastName: '', email: '', password: '', profileType: 'applicant', isRealEstate: false, phone: 1234567980, direction: false }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => (
          <SignUpTemplate
            {...props}
            image={signUpImage}
            formFields={formFields(endIconPassword, handleIconAction, typePassword)}
          />
        )}
      </Formik>
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ doSignUpRequest }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPage);
