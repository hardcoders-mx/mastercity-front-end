import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import PropertiesMapPage from './PropertiesMapPage';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<PropertiesMapPage />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<PropertiesMapPage />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<PropertiesMapPage className="PropertiesMapPage"/>);
  expect(component.find('div').hasClass('PropertiesMapPage')).toBe(true);
});
