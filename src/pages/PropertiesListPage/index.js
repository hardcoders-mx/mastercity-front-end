import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { doPropertiesRequest } from '../../store/modules/properties/actions';
import PropertiesListPage from './PropertiesListPage';

const PropertiesContainer = (props) => {
  const [params, setParams] = useState(localStorage.getItem("params") || {});
  const { doPropertiesRequest } = props;

  useEffect(() => {
    doPropertiesRequest(params);
  }, [params]);

  return <PropertiesListPage setParams={setParams} />
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ doPropertiesRequest }, dispatch);

export default connect(null, mapDispatchToProps)(PropertiesContainer);
