import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import withHandlingRendering from '../../containers/withHandlingRendering';
import Card from '../../components/organisms/Card';
import Modal from '../../components/molecules/Modal';
import './PropertiesListPage.scss';

const PropertiesListPage = ({ data, setParams }) => {
  const history = useHistory();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [contactInfo, setContactInfo] = useState({
    firstName: '',
    lastName: '',
    email: '',
    phone: '55 11122 3344',
    isRealEstate: false,
  });
  const { email, phone, firstName, lastName, isRealEstate } = contactInfo;

  const handleShow = (id) => () => {
    history.push(`/properties/${id}`);
  };

  const handleContact = (offerer) => () => {
    setIsModalOpen(true);
    setContactInfo({
      ...contactInfo,
      ...offerer
    });
  };

  return (
    <div className="PropertiesListPage">
      {data.map((property) => {
        const { _id, mediaFiles, price, squareMeters, rooms, bathrooms, parking, address, offerer } = property;
        const { street, colony, state, townHall } = address;
        const { firstName, isRealEstate, avatar } = offerer;
        const cardProps = {
          // coin,
          avatar,
          image: mediaFiles[0].secure_url,
          price: price,
          offerer: isRealEstate ? `Real State - ${firstName}` : 'Direct Deal',
          description: `${street} ${colony} ${state} ${townHall}`,
          featureOne: `${squareMeters} m2`,
          featureTwo: `${rooms} Rooms`,
          featureThree: `${bathrooms} Bathrooms`,
          featureFour: `Parking: ${parking ? 'Sí' : 'No'}`,
          handleShow: handleShow(_id),
          handleContact: handleContact(offerer),
        };
        return <Card {...cardProps} />;
      })}
      {isModalOpen && (
        <Modal onClose={() => setIsModalOpen(false)} style={{ width: 400, textAlign: 'center' }}>
          <p><b>Offerer:</b> {isRealEstate ? `Real State - ${firstName} ${lastName}` : `Direct Deal - ${firstName} ${lastName}`}</p>
          <p><b>Email:</b> {email}</p>
          <p><b>Phone Number:</b> {phone}</p>
        </Modal>
      )}
    </div>
  );
};

const mapStateToProps = ({ properties }, ownProps) => ({
  error: properties.error,
  loading: properties.loading,
  message: properties.message,
  data: properties.body && properties.body.properties,
});

export default connect(mapStateToProps)(withHandlingRendering(PropertiesListPage));
