import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import BrokerPage from './BrokerPage';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<BrokerPage />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<BrokerPage />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<BrokerPage className="BrokerPage"/>);
  expect(component.find('div').hasClass('BrokerPage')).toBe(true);
});
