import React from 'react';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { mdiMapMarker, mdiCrosshairsGps, mdiHome } from '@mdi/js';
import SearchTemplate from '../../components/templates/SearchTemplate';
import endpoints from '../../services/api/endpoints';
import image from '../../images/search.png';
import './SearchPage.scss';

const validationSchema = object().shape({
  address: string().required().label('Location'),
  propertyType: string().oneOf(['house', 'office', 'department', 'studio']).required().label('Property Type'),
  operationType: string().oneOf(['buy', 'rent']).required().label('Operation Type'),
});

const formFields = [
  {
    name: 'address',
    label: 'Location',
    type: 'text',
    component: 'text',
    startIcon: mdiMapMarker,
    endIcon: mdiCrosshairsGps,
  },
  {
    name: 'propertyType',
    label: 'Property Type',
    options: [
      {
        value: 'house',
        label: 'House',
      },
      {
        value: 'office',
        label: 'Office',
      },
      {
        value: 'department',
        label: 'Department',
      },
      {
        value: 'studio',
        label: 'Studio',
      },
    ],
    component: 'select',
    startIcon: mdiHome,
  },
  {
    name: 'operationType',
    component: 'radioAttached',
    options: [
      { label: 'For Sale', value: 'buy' },
      { label: 'To Rent', value: 'rent' },
    ],
  },
];

const SearchPage = ({ history }) => {
  const handleSubmit = (values) => {
    localStorage.setItem('params', JSON.stringify(values));
    history.push(endpoints.properties.create);
  };

  return (
    <div className="SearchPage">
      <Formik
        initialValues={{ address: '', propertyType: '', operationType: 'buy' }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => <SearchTemplate image={image} formFields={formFields} {...props} />}
      </Formik>
    </div>
  );
};

export default SearchPage;
