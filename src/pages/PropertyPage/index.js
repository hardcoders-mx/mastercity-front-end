import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { doPropertyRequest } from '../../store/modules/property/actions';
import PropertyPage from './PropertyPage';

const PropertyContainer = (props) => {
  const { doPropertyRequest } = props;
  const { id } = props.match.params;

  useEffect(() => {
    doPropertyRequest(id);
  }, [id]);

  return <PropertyPage />;
};

const mapDispatchToProps = (dispatch) => bindActionCreators({ doPropertyRequest }, dispatch);

export default connect(null, mapDispatchToProps)(PropertyContainer);
