import React, { useState } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import withHandlingRendering from '../../containers/withHandlingRendering';
import PropertyTemplate from '../../components/templates/PropertyTemplate';

const PropertyPage = ({ data }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [property] = data;

  const handleOpenModal = () => setIsModalOpen(true);

  const handleCloseModal = () => setIsModalOpen(false);

  const propertyProps = {
    property,
    isModalOpen,
    onClick: handleOpenModal,
    onClose: handleCloseModal,
  };

  return (
    <div className="PropertyPage">
      <PropertyTemplate {...propertyProps} />
    </div>
  );
};

const mapStateToProps = ({ property }, ownProps) => ({
  error: property.error,
  loading: property.loading,
  message: property.message,
  data: !isEmpty(property.body) ? [property.body] : [],
});

export default connect(mapStateToProps)(withHandlingRendering(PropertyPage));