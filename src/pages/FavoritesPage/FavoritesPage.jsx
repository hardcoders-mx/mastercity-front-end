import React from 'react';
import { connect } from 'react-redux';
import FavoritesTemplate from '../../components/templates/FavoritesTemplate';
import withHandlingRendering from '../../containers/withHandlingRendering';
import './FavoritesPage.scss';

const FavoritesPage = (props) => {
  const { data: favorites } = props;
  console.log("FavoritesPage -> favorites", favorites)

  return (
    <div className="FavoritesPage">
      <FavoritesTemplate favorites={favorites} />
    </div>
  );
};

const mapStateToProps = ({ favorites }, ownProps) => ({
  error: favorites.error,
  loading: favorites.loading,
  message: favorites.message,
  data: favorites.body,
});

export default connect(mapStateToProps)(withHandlingRendering(FavoritesPage));
