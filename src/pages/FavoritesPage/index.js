import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { doFavoritesRequest } from '../../store/modules/favorites/actions';
import FavoritesPage from './FavoritesPage';

const FavoritesContainer = ({ userId, doFavoritesRequest }) => {
  console.log("FavoritesContainer -> userId", userId)
  useEffect(() => {
    doFavoritesRequest({ user: userId });
  }, [userId]);

  return <FavoritesPage />;
};

const mapStateToProps = (state) => ({
  userId: state.auth && state.auth.user && state.auth.user._id,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ doFavoritesRequest }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesContainer);
