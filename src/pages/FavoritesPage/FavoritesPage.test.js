import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import FavoritesPage from './FavoritesPage';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<FavoritesPage />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

// (2) Checking DOM element
it('Should have single div', () => {
  const component = shallow(<FavoritesPage />);
  expect(component.find('div').length).toBe(1);
});

// (3) Checking class
it('Should have the correct class name', ()=> {
  const component = shallow(<FavoritesPage className="FavoritesPage"/>);
  expect(component.find('div').hasClass('FavoritesPage')).toBe(true);
});
