/* PLOP_INJECT_IMPORT */
import DashboardPage from './DashboardPage';
import AdminPage from './AdminPage';
import BrokerPage from './BrokerPage';
import RealEstatePage from './RealEstatePage';
import AccountPage from './AccountPage';
import FavoritesPage from './FavoritesPage';
import SearchPage from './SearchPage';
import PropertyMapPage from './PropertyMapPage';
import PropertyPage from './PropertyPage';
import PropertyRegistrationPage from './PropertyRegistrationPage';
import PropertiesMapPage from './PropertiesMapPage';
import PropertiesListPage from './PropertiesListPage';
import AccountRecoveryPage from './AccountRecoveryPage';
import SignUpPage from './SignUpPage';
import LoginPage from './LoginPage';
import HomePage from './HomePage';
import WelcomePage from './WelcomePage';

export {
  /* PLOP_INJECT_EXPORT */
  DashboardPage,
  AdminPage,
  BrokerPage,
  RealEstatePage,
  AccountPage,
  FavoritesPage,
  SearchPage,
  PropertyMapPage,
  PropertyPage,
  PropertyRegistrationPage,
  PropertiesMapPage,
  PropertiesListPage,
  AccountRecoveryPage,
  SignUpPage,
  LoginPage,
  HomePage,
  WelcomePage,
};
