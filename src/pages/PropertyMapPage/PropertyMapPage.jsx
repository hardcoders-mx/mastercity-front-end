import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const PropertyMapPage = (props) => {
  const [state, setState] = useState('initialState')
  const {} = props;

  return (
    <div className="PropertyMapPage">
      PropertyMapPage
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({

});

const mapDispatchToProps = (dispatch) => (bindActionCreators({

}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(PropertyMapPage);
