import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const PropertyRegistrationPage = (props) => {
  const [state, setState] = useState('initialState')
  const {} = props;

  return (
    <div className="PropertyRegistrationPage">
      PropertyRegistrationPage
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({

});

const mapDispatchToProps = (dispatch) => (bindActionCreators({

}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(PropertyRegistrationPage);
