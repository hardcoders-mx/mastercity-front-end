import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Provider, ReactReduxContext } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import history from '../store/history';
import store from '../store/create';
import { routes } from '../routes';
import ModalContextProvider from '../containers/ModalContextProvider';
import './App.scss';
import { NavBar } from '../components/organisms/NavBar';
import NavMenu from '../components/organisms/NavMenu';

const routeComponents = Object.values(routes).map(({ path, component }, key) => (
  <Route exact path={path} component={component} key={key} />
));

function App() {
  return (
    <div className="App">
      <Provider store={store} context={ReactReduxContext}>
        <ConnectedRouter history={history} context={ReactReduxContext}>
          <NavMenu />
          <ModalContextProvider>
            <Switch>{routeComponents}</Switch>
          </ModalContextProvider>
          <NavBar />
        </ConnectedRouter>
      </Provider>
    </div>
  );
}

export default App;
