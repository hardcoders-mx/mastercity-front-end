import React from 'react';
import renderer from 'react-test-renderer';
import App from './App';

// (1) Snapshot testing
it('Should render correctly', () => {
  const component = renderer.create(<App />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});