import axios from 'axios';
import config from '../config';

const client = axios.create({
  baseURL: config.baseUrl,
});

const authClient = axios.create({
  baseURL: config.baseUrl,
});

authClient.interceptors.request.use(
  (setup) => {
    const { token } = JSON.parse(localStorage.getItem('user'));
    console.log('token', token);
    if (token) {
      setup.headers.Authorization = `Bearer ${token}`;
    }
    return setup;
  },
  (error) => Promise.reject(error),
);

export const basicAuth = (url, { username, password }) => {
  return client({
    url,
    method: 'POST',
    auth: { username, password },
  });
};

export const getData = (url, params = {}) => {
  return client({ url, params, method: 'GET' });
};

export const postData = (url, data = {}) => {
  return client({ url, data, method: 'POST' });
};

export const putData = (url, data = {}) => {
  return client({ url, data, method: 'PUT' });
};

export const patchData = (url, data = {}) => {
  return client({ url, data, method: 'PATCH' });
};

export const deleteData = (url, data = {}) => {
  return client({ url, data, method: 'DELETE' });
};

export const getDataAuth = (url, params = {}) => {
  return authClient({ url, params, method: 'GET' });
};

export const postDataAuth = (url, data = {}) => {
  return authClient({ url, data, method: 'POST' });
};

export const putDataAuth = (url, data = {}) => {
  return authClient({ url, data, method: 'PUT' });
};

export const patchDataAuth = (url, data = {}) => {
  return authClient({ url, data, method: 'PATCH' });
};

export const deleteDataAuth = (url, data = {}) => {
  return authClient({ url, data, method: 'DELETE' });
};
