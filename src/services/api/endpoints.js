/*
  create: '', // new
  index: '',
  show: '',
  update: '', // edit
  delete: '',
*/

const endpoints = {
  auth: {
    login: '/auth/sign-in',
    signup: '/auth/sign-up',
    signout: '/auth/sign-out',
  },
  users: {
    crud: '/users/:id',
  },
  properties: {
    create: '/properties',
    crud: '/properties',
  },
  favorites: {
    create: '/favorites',
    crud: '/favorites',
  },
};

export default endpoints;
