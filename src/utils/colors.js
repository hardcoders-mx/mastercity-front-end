export const primaryMain = '#0055b0';
export const primaryLight = '#5581e3';
export const primaryDark = '#002d80';
export const primaryContrastText = '#ffffff';

export const secondaryMain = '#fcac1e';
export const secondaryLight = '#ffde57';
export const secondaryDark = '#c47d00';
export const secondaryContrastText = '#000000';

export const success = '#43a047';
export const danger = '#e53935';
export const warning = '#ffb300';
export const informational = '#1e88e5';

export const black = '#333333';
export const gray1 = '#4f4f4f';
export const gray2 = '#828282';
export const gray3 = '#bdbdbd';
export const gray4 = '#e0e0e0';
export const white = '#f2f2f2';
