import * as pages from './pages';

const {
  HomePage,
  LoginPage,
  SignUpPage,
  AccountRecoveryPage,
  PropertiesListPage,
  PropertiesMapPage,
  PropertyRegistrationPage,
  PropertyPage,
  PropertyMapPage,
  SearchPage,
  FavoritesPage,
  AccountPage,
  RealEstatePage,
  BrokerPage,
  AdminPage,
  DashboardPage,
  WelcomePage,
} = pages;

export const routes = {
  home: {
    path: '/',
    component: HomePage,
  },
  login: {
    path: '/login',
    component: LoginPage,
  },
  signup: {
    path: '/signup',
    component: SignUpPage,
  },
  recovery: {
    path: '/recovery',
    component: AccountRecoveryPage,
  },
  properties: {
    path: '/properties',
    component: PropertiesListPage,
  },
  propertiesMap: {
    path: '/properties/map',
    component: PropertiesMapPage,
  },
  propertyRegistration: {
    path: '/properties/registration',
    component: PropertyRegistrationPage,
  },
  property: {
    path: '/properties/:id',
    component: PropertyPage,
  },
  propertyMap: {
    path: '/properties/:id/map',
    component: PropertyMapPage,
  },
  search: {
    path: '/search',
    component: SearchPage,
  },
  favorites: {
    path: '/favorites',
    component: FavoritesPage,
  },
  account: {
    path: '/account',
    component: AccountPage,
  },
  realEstate: {
    path: '/real-estate',
    component: RealEstatePage,
  },
  broker: {
    path: '/broker',
    component: BrokerPage,
  },
  admin: {
    path: '/admin',
    component: AdminPage,
  },
  dashboard: {
    path: '/dashboard',
    component: DashboardPage,
  },
  welcome: {
    path: '/welcome',
    component: WelcomePage,
  },
};
