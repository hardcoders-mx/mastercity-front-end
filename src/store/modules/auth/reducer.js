import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT,
  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
} from './types';

const user = JSON.parse(localStorage.getItem('user'));

let initialState = {
  body: {},
  isLogged: false,
  loading: false,
  error: false,
  message: '',
};

if (user) {
  initialState = {
    ...initialState,
    ...user,
    isLogged: true,
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
    case SIGN_UP_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case LOGIN_SUCCESS:
    case SIGN_UP_SUCCESS:
      return {
        ...state,
        ...action.payload.data,
        isLogged: true,
        loading: false,
      };
    case LOGIN_FAILURE:
    case SIGN_UP_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.payload,
      };
    case LOGOUT:
      return { ...initialState };
    default:
      return state;
  }
}
