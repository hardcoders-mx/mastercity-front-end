import { action } from '../common';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT,
  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
} from './types';

export const doSignUpRequest = (payload) => action(SIGN_UP_REQUEST, payload);

export const doSignUpSuccess = (payload) => action(SIGN_UP_SUCCESS, payload);

export const doSignUpFailure = (payload) => action(SIGN_UP_FAILURE, payload);

export const doLoginRequest = (payload) => action(LOGIN_REQUEST, payload);

export const doLoginSuccess = (payload) => action(LOGIN_SUCCESS, payload);

export const doLoginFailure = (payload) => action(LOGIN_FAILURE, payload);

export const doLogout = () => action(LOGOUT);
