export const LOGIN_REQUEST = 'USERS_LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'USERS_LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'USERS_LOGIN_FAILURE';

export const SIGN_UP_REQUEST = 'USERS_SIGN_UP_REQUEST';
export const SIGN_UP_SUCCESS = 'USERS_SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'USERS_SIGN_UP_FAILURE';

export const LOGOUT = 'USERS_LOGOUT';

export const GET_ALL_REQUEST = 'USERS_GET_ALL_REQUEST';
export const GET_ALL_SUCCESS = 'USERS_GET_ALL_SUCCESS';
export const GET_ALL_FAILURE = 'USERS_GET_ALL_FAILURE';
