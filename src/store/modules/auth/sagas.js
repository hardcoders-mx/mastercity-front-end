// import { delay } from 'redux-saga'
import { put, takeLatest, call } from 'redux-saga/effects';
import { SIGN_UP_REQUEST, LOGIN_REQUEST } from './types';
import { doSignUpSuccess, doSignUpFailure, doLoginSuccess, doLoginFailure } from './actions';
import { postData, basicAuth } from '../../../services/api/functions';
import endpoints from '../../../services/api/endpoints';

const clientCreateUser = (payload) => postData(endpoints.auth.signup, payload);

/* const clientLoginUser = (payload) => {
  const token = Buffer.from(`${payload.email}:${payload.password}`, 'utf8').toString('base64');
  return basicAuth(endpoints.auth.login, token);
}; */

const clientLoginUser = (payload) => basicAuth(endpoints.auth.login, payload);

export function* createUser({ payload }) {
  try {
    // dispatch success action here
    const response = yield call(clientCreateUser, payload);
    yield put(doSignUpSuccess(response));
  } catch (error) {
    // dispatch error action here
    console.log('function*createUser -> error', error);
    yield put(doSignUpFailure(error));
  }
}

export function* loginUser({ payload }) {
  try {
    const response = yield call(clientLoginUser, payload);
    localStorage.setItem('user', JSON.stringify(response.data));
    yield put(doLoginSuccess(response));
  } catch (error) {
    console.log('function*loginUser -> error', error);
    yield put(doLoginFailure(error));
  }
}

// Watches for specific action, and then executes the related saga
export function* watchSignUpAsync() {
  yield takeLatest(SIGN_UP_REQUEST, createUser);
}

export function* watchLoginAsync() {
  yield takeLatest(LOGIN_REQUEST, loginUser);
}

// export only watcher sagas in one variable
export const sagas = [watchSignUpAsync, watchLoginAsync];
