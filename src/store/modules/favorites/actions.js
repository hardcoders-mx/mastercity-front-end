import { action } from '../common';
import { FAVORITES_REQUEST, FAVORITES_SUCCESS, FAVORITES_FAILURE } from './types';

export const doFavoritesRequest = (payload) => action(FAVORITES_REQUEST, payload);

export const doFavoritesSuccess = (payload) => action(FAVORITES_SUCCESS, payload);

export const doFavoritesFailure = (payload) => action(FAVORITES_FAILURE, payload);
