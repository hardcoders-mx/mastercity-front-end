import { FAVORITES_REQUEST, FAVORITES_SUCCESS, FAVORITES_FAILURE } from './types';

let initialState = {
  body: {},
  loading: false,
  error: false,
  message: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FAVORITES_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FAVORITES_SUCCESS:
      return {
        ...state,
        ...action.payload.data,
        loading: false,
      };
    case FAVORITES_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.payload,
      };
    default:
      return state;
  }
}
