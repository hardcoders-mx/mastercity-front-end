import { put, call, takeEvery } from 'redux-saga/effects';
import { FAVORITES_REQUEST } from './types';
import { doFavoritesSuccess, doFavoritesFailure } from './actions';
import { getDataAuth } from '../../../services/api/functions';
import endpoints from '../../../services/api/endpoints';

const { crud } = endpoints.favorites;

const clientFavoritesRead = (params) => getDataAuth(crud, params);

export function* getFavorites({ payload }) {
  try {
    const response = yield call(clientFavoritesRead, payload);
    yield put(doFavoritesSuccess(response));
  } catch (error) {
    console.log('function*getFavorites -> error', error);
    if (error.message) {
      yield put(doFavoritesFailure(error.message));
    } else {
      yield put(doFavoritesFailure(error));
    }
  }
}

export function* watchGetFavoritesAsync() {
  yield takeEvery(FAVORITES_REQUEST, getFavorites);
}

export const sagas = [watchGetFavoritesAsync];
