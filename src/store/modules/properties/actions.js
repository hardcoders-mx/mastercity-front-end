import { action } from '../common';
import { PROPERTIES_REQUEST, PROPERTIES_SUCCESS, PROPERTIES_FAILURE } from './types';

export const doPropertiesRequest = (payload) => action(PROPERTIES_REQUEST, payload);

export const doPropertiesSuccess = (payload) => action(PROPERTIES_SUCCESS, payload);

export const doPropertiesFailure = (payload) => action(PROPERTIES_FAILURE, payload);
