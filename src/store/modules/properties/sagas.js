import { put, call, takeEvery } from 'redux-saga/effects';
import { PROPERTIES_REQUEST } from './types';
import { doPropertiesSuccess, doPropertiesFailure } from './actions';
import { getData } from '../../../services/api/functions';
import endpoints from '../../../services/api/endpoints';

const { create } = endpoints.properties;

const clientPropertiesRead = (data) => getData(create, data);

export function* getProperties({ payload }) {
  try {
    const response = yield call(clientPropertiesRead, payload);
    yield put(doPropertiesSuccess(response));
  } catch (error) {
    console.log('function*getProperties -> error', error);
    if (error.message) {
      yield put(doPropertiesFailure(error.message));
    } else {
      yield put(doPropertiesFailure(error));
    }
  }
}

export function* watchGetPropertiesAsync() {
  yield takeEvery(PROPERTIES_REQUEST, getProperties);
}

export const sagas = [watchGetPropertiesAsync];
