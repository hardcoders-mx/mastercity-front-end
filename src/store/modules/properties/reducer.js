import { PROPERTIES_REQUEST, PROPERTIES_SUCCESS, PROPERTIES_FAILURE } from './types';

let initialState = {
  body: {},
  loading: false,
  error: false,
  message: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case PROPERTIES_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case PROPERTIES_SUCCESS:
      return {
        ...state,
        ...action.payload.data,
        loading: false,
      };
    case PROPERTIES_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.payload,
      };
    default:
      return state;
  }
}
