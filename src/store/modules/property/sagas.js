import { put, call, takeEvery } from 'redux-saga/effects';
import { PROPERTY_REQUEST } from './types';
import { doPropertySuccess, doPropertyFailure } from './actions';
import { getData } from '../../../services/api/functions';
import endpoints from '../../../services/api/endpoints';

const { crud } = endpoints.properties;

const clientPropertyRead = (id) => getData(`${crud}/${id}`);

export function* getProperty({ payload }) {
  try {
    const response = yield call(clientPropertyRead, payload);
    yield put(doPropertySuccess(response));
  } catch (error) {
    console.log('function*getProperty -> error', error);
    if (error.message) {
      yield put(doPropertyFailure(error.message));
    } else {
      yield put(doPropertyFailure(error));
    }
  }
}

export function* watchGetPropertyAsync() {
  yield takeEvery(PROPERTY_REQUEST, getProperty);
}

export const sagas = [watchGetPropertyAsync];
