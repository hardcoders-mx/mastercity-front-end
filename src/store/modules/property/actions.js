import { action } from '../common';
import { PROPERTY_REQUEST, PROPERTY_SUCCESS, PROPERTY_FAILURE } from './types';

export const doPropertyRequest = (payload) => action(PROPERTY_REQUEST, payload);

export const doPropertySuccess = (payload) => action(PROPERTY_SUCCESS, payload);

export const doPropertyFailure = (payload) => action(PROPERTY_FAILURE, payload);
