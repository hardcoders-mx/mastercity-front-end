import { PROPERTY_REQUEST, PROPERTY_SUCCESS, PROPERTY_FAILURE } from './types';

let initialState = {
  body: {},
  loading: false,
  error: false,
  message: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case PROPERTY_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case PROPERTY_SUCCESS:
      return {
        ...state,
        ...action.payload.data,
        loading: false,
      };
    case PROPERTY_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.payload,
      };
    default:
      return state;
  }
}
