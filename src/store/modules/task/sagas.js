import { put, takeEvery, call } from 'redux-saga/effects';
// import { delay } from 'redux-saga'
import { doReceiveTasks } from './actions';
import { REQUEST_TASKS } from './types';
import { postData } from '../../../services/api/functions';

export function* getAllTasks() {
  try {
    const data = yield call(postData('/api/tasks'));
    yield put(doReceiveTasks(data));
  } catch (err) {
    // dispatch error action here
    console.log(err);
  }
}

// Watches for specific action, and then executes the related saga
export function* watchGetAllTasksAsync() {
  yield takeEvery(REQUEST_TASKS, getAllTasks);
}

// export only watcher sagas in one variable
export const sagas = [watchGetAllTasksAsync];
