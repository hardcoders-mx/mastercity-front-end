import { CREATE_TASK, READ_TASK, UPDATE_TASK, DELETE_TASK, REQUEST_TASKS, RECEIVE_TASKS } from './types';

export const doAddTask = (task) => ({
  type: CREATE_TASK,
  payload: task,
});

export const doShowTask = (task) => ({
  type: READ_TASK,
  task,
});

export const doEditTask = (task) => ({
  type: UPDATE_TASK,
  task,
});

export const doDeleteTask = (task) => ({
  type: DELETE_TASK,
  task,
});

export const doRequestTasks = function () {
  return {
    type: REQUEST_TASKS,
  };
};

export const doReceiveTasks = function (tasks) {
  return {
    type: RECEIVE_TASKS,
    payload: tasks,
  };
};
