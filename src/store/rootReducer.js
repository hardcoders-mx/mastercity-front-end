import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from './history';
import taskReducer from './modules/task';
import auth from './modules/auth';
import properties from './modules/properties';
import property from './modules/property';
import favorites from './modules/favorites';

export default combineReducers({
  router: connectRouter(history),
  toDo: taskReducer,
  auth,
  properties,
  property,
  favorites,
});
