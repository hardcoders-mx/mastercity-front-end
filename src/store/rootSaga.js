import { sagas as taskSagas } from './modules/task';
import { sagas as authSagas } from './modules/auth';
import { sagas as propertiesSagas } from './modules/properties';
import { sagas as propertySagas } from './modules/property';
import { sagas as favoritesSagas } from './modules/favorites';
import { /* takeEvery, takeLatest, */ fork, all } from 'redux-saga/effects';

const allSagas = [...taskSagas, ...authSagas, ...propertiesSagas, ...propertySagas, ...favoritesSagas];

// start all sagas in parallel
export default function* rootSaga() {
  yield all(allSagas.map((saga) => fork(saga)));
}
