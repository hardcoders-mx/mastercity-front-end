## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Git Flow

- Branch name for production releases: **[master]**
- Branch name for "next release" development: **[develop]**

### Supporting branch prefixes

- Feature branches: **[feature/]**
- Bugfix branches: **[bugfix/]**
- Release branches: **[release/]**
- Hotfix branches: **[hotfix/]**
- Support branches: **[support/]**
- Version tag: **vXX.XX.XX**

## Task Weight

Allowed Numbers: 0, 1, 2, 3, 5, 8, 13, 21.

If a task has a weight greater than 21, this task should be divided into smaller tasks.

## Styles

- Preprocessor: Sass
- Methodology: BEM
- Naming convention: [React Style](https://en.bem.info/methodology/naming-convention/#react-style)

```bash
BlockName-ElemName_modName_modVal
```

## Folder Structure - src/

```ASCII
src/
┣ App/
┃ ┣ App.jsx
┃ ┣ App.scss
┃ ┣ App.test.js
┃ ┗ index.js
┣ api/
┃ ┗ functions.js
┣ components/
┃ ┣ atoms/
┃ ┃ ┣ ProgressState/
┃ ┃ ┃ ┗ index.jsx
┃ ┃ ┗ Typography/
┃ ┃ ┃ ┣ Typography.scss
┃ ┃ ┃ ┗ index.jsx
┃ ┣ molecules/
┃ ┣ organisms/
┃ ┣ templates/
┃ ┃ ┣ AccountRecoveryTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ AccountRecoveryTemplate.test.js.snap
┃ ┃ ┃ ┣ AccountRecoveryTemplate.jsx
┃ ┃ ┃ ┣ AccountRecoveryTemplate.scss
┃ ┃ ┃ ┣ AccountRecoveryTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ AccountTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ AccountTemplate.test.js.snap
┃ ┃ ┃ ┣ AccountTemplate.jsx
┃ ┃ ┃ ┣ AccountTemplate.scss
┃ ┃ ┃ ┣ AccountTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ AdminTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ AdminTemplate.test.js.snap
┃ ┃ ┃ ┣ AdminTemplate.jsx
┃ ┃ ┃ ┣ AdminTemplate.scss
┃ ┃ ┃ ┣ AdminTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ BrokerTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ BrokerTemplate.test.js.snap
┃ ┃ ┃ ┣ BrokerTemplate.jsx
┃ ┃ ┃ ┣ BrokerTemplate.scss
┃ ┃ ┃ ┣ BrokerTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ DashboardTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ DashboardTemplate.test.js.snap
┃ ┃ ┃ ┣ DashboardTemplate.jsx
┃ ┃ ┃ ┣ DashboardTemplate.scss
┃ ┃ ┃ ┣ DashboardTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ FavoritesTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ FavoritesTemplate.test.js.snap
┃ ┃ ┃ ┣ FavoritesTemplate.jsx
┃ ┃ ┃ ┣ FavoritesTemplate.scss
┃ ┃ ┃ ┣ FavoritesTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ HomeTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ HomeTemplate.test.js.snap
┃ ┃ ┃ ┣ HomeTemplate.jsx
┃ ┃ ┃ ┣ HomeTemplate.scss
┃ ┃ ┃ ┣ HomeTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ LoginTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ LoginTemplate.test.js.snap
┃ ┃ ┃ ┣ LoginTemplate.jsx
┃ ┃ ┃ ┣ LoginTemplate.scss
┃ ┃ ┃ ┣ LoginTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ PropertiesListTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ PropertiesListTemplate.test.js.snap
┃ ┃ ┃ ┣ PropertiesListTemplate.jsx
┃ ┃ ┃ ┣ PropertiesListTemplate.scss
┃ ┃ ┃ ┣ PropertiesListTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ PropertiesMapTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ PropertiesMapTemplate.test.js.snap
┃ ┃ ┃ ┣ PropertiesMapTemplate.jsx
┃ ┃ ┃ ┣ PropertiesMapTemplate.scss
┃ ┃ ┃ ┣ PropertiesMapTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ PropertyMapTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ PropertyMapTemplate.test.js.snap
┃ ┃ ┃ ┣ PropertyMapTemplate.jsx
┃ ┃ ┃ ┣ PropertyMapTemplate.scss
┃ ┃ ┃ ┣ PropertyMapTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ PropertyRegistrationTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ PropertyRegistrationTemplate.test.js.snap
┃ ┃ ┃ ┣ PropertyRegistrationTemplate.jsx
┃ ┃ ┃ ┣ PropertyRegistrationTemplate.scss
┃ ┃ ┃ ┣ PropertyRegistrationTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ PropertyTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ PropertyTemplate.test.js.snap
┃ ┃ ┃ ┣ PropertyTemplate.jsx
┃ ┃ ┃ ┣ PropertyTemplate.scss
┃ ┃ ┃ ┣ PropertyTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ RealEstateTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ RealEstateTemplate.test.js.snap
┃ ┃ ┃ ┣ RealEstateTemplate.jsx
┃ ┃ ┃ ┣ RealEstateTemplate.scss
┃ ┃ ┃ ┣ RealEstateTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┣ SearchTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ SearchTemplate.test.js.snap
┃ ┃ ┃ ┣ SearchTemplate.jsx
┃ ┃ ┃ ┣ SearchTemplate.scss
┃ ┃ ┃ ┣ SearchTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┃ ┗ SignUpTemplate/
┃ ┃ ┃ ┣ __snapshots__/
┃ ┃ ┃ ┃ ┗ SignUpTemplate.test.js.snap
┃ ┃ ┃ ┣ SignUpTemplate.jsx
┃ ┃ ┃ ┣ SignUpTemplate.scss
┃ ┃ ┃ ┣ SignUpTemplate.test.js
┃ ┃ ┃ ┗ index.js
┃ ┗ index.js
┣ containers/
┃ ┗ withHandlingRendering.jsx
┣ context/
┣ hooks/
┣ layouts/
┣ pages/
┃ ┣ AccountPage/
┃ ┃ ┣ AccountPage.jsx
┃ ┃ ┣ AccountPage.scss
┃ ┃ ┣ AccountPage.test.js
┃ ┃ ┗ index.js
┃ ┣ AccountRecoveryPage/
┃ ┃ ┣ AccountRecoveryPage.jsx
┃ ┃ ┣ AccountRecoveryPage.scss
┃ ┃ ┣ AccountRecoveryPage.test.js
┃ ┃ ┗ index.js
┃ ┣ AdminPage/
┃ ┃ ┣ AdminPage.jsx
┃ ┃ ┣ AdminPage.scss
┃ ┃ ┣ AdminPage.test.js
┃ ┃ ┗ index.js
┃ ┣ BrokerPage/
┃ ┃ ┣ BrokerPage.jsx
┃ ┃ ┣ BrokerPage.scss
┃ ┃ ┣ BrokerPage.test.js
┃ ┃ ┗ index.js
┃ ┣ DashboardPage/
┃ ┃ ┣ DashboardPage.jsx
┃ ┃ ┣ DashboardPage.scss
┃ ┃ ┣ DashboardPage.test.js
┃ ┃ ┗ index.js
┃ ┣ FavoritesPage/
┃ ┃ ┣ FavoritesPage.jsx
┃ ┃ ┣ FavoritesPage.scss
┃ ┃ ┣ FavoritesPage.test.js
┃ ┃ ┗ index.js
┃ ┣ HomePage/
┃ ┃ ┣ HomePage.jsx
┃ ┃ ┣ HomePage.scss
┃ ┃ ┣ HomePage.test.js
┃ ┃ ┗ index.js
┃ ┣ LoginPage/
┃ ┃ ┣ LoginPage.jsx
┃ ┃ ┣ LoginPage.scss
┃ ┃ ┣ LoginPage.test.js
┃ ┃ ┗ index.js
┃ ┣ PropertiesListPage/
┃ ┃ ┣ PropertiesListPage.jsx
┃ ┃ ┣ PropertiesListPage.scss
┃ ┃ ┣ PropertiesListPage.test.js
┃ ┃ ┗ index.js
┃ ┣ PropertiesMapPage/
┃ ┃ ┣ PropertiesMapPage.jsx
┃ ┃ ┣ PropertiesMapPage.scss
┃ ┃ ┣ PropertiesMapPage.test.js
┃ ┃ ┗ index.js
┃ ┣ PropertyMapPage/
┃ ┃ ┣ PropertyMapPage.jsx
┃ ┃ ┣ PropertyMapPage.scss
┃ ┃ ┣ PropertyMapPage.test.js
┃ ┃ ┗ index.js
┃ ┣ PropertyPage/
┃ ┃ ┣ PropertyPage.jsx
┃ ┃ ┣ PropertyPage.scss
┃ ┃ ┣ PropertyPage.test.js
┃ ┃ ┗ index.js
┃ ┣ PropertyRegistrationPage/
┃ ┃ ┣ PropertyRegistrationPage.jsx
┃ ┃ ┣ PropertyRegistrationPage.scss
┃ ┃ ┣ PropertyRegistrationPage.test.js
┃ ┃ ┗ index.js
┃ ┣ RealEstatePage/
┃ ┃ ┣ RealEstatePage.jsx
┃ ┃ ┣ RealEstatePage.scss
┃ ┃ ┣ RealEstatePage.test.js
┃ ┃ ┗ index.js
┃ ┣ SearchPage/
┃ ┃ ┣ SearchPage.jsx
┃ ┃ ┣ SearchPage.scss
┃ ┃ ┣ SearchPage.test.js
┃ ┃ ┗ index.js
┃ ┣ SignUpPage/
┃ ┃ ┣ SignUpPage.jsx
┃ ┃ ┣ SignUpPage.scss
┃ ┃ ┣ SignUpPage.test.js
┃ ┃ ┗ index.js
┃ ┗ index.js
┣ store/
┃ ┣ modules/
┃ ┃ ┣ [moduleName]/
┃ ┃ ┃ ┣ actions.js
┃ ┃ ┃ ┣ index.js
┃ ┃ ┃ ┣ reducers.js
┃ ┃ ┃ ┣ sagas.js
┃ ┃ ┃ ┗ types.js
┃ ┃ ┗ index.js
┃ ┣ create.js
┃ ┣ history.js
┃ ┣ rootReducer.js
┃ ┗ rootSaga.js
┣ utils/
┣ index.js
┣ index.scss
┣ routes.js
┗ setupTests.js
```
